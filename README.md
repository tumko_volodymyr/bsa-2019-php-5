# Binary Studio Academy 2019

## Домашнее задание #5 

***

### Установка

Установка показана в рабочем окружении OS Linux:

```bash
git clone https://tumko_volodymyr@bitbucket.org/tumko_volodymyr/bsa-2019-php-5.git
cd bsa-2019-php-5
composer install
```

### Запуск

Разрешается использовать любое тестовое окружение с PHP >7.1.

Или использовать предоставленное docker окружение. Для этого достаточно сбилдить image:

```
docker-compose build
```

Установить зависимости:

```
docker-compose run --rm composer install
```

Запустить приложение:

```
docker-compose run --rm php php game.php
```

Запустить тесты:

```
docker-compose run --rm php ./vendor/bin/phpunit
```
