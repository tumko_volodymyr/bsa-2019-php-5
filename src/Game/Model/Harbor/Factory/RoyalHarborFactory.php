<?php


namespace BinaryStudioAcademy\Game\Model\Harbor\Factory;


use BinaryStudioAcademy\Game\Contracts\Model\Harbor;
use BinaryStudioAcademy\Game\Model\Harbor\StrangeHarbor;
use BinaryStudioAcademy\Game\Model\Ship\ShipFactory;

class RoyalHarborFactory implements HarborFactory
{

    function createHarbor(int $number, string $name): Harbor
    {
        return new StrangeHarbor($number, $name, ShipFactory::createRoyalShip());
    }
}