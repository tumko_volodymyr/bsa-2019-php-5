<?php


namespace BinaryStudioAcademy\Game\Model\Harbor\Factory;


use BinaryStudioAcademy\Game\Contracts\Model\Harbor;

interface HarborFactory
{
    function createHarbor(int $number, string $name): Harbor ;
}