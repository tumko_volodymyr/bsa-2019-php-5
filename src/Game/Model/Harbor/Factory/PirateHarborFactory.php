<?php


namespace BinaryStudioAcademy\Game\Model\Harbor\Factory;


use BinaryStudioAcademy\Game\Contracts\Model\Harbor;
use BinaryStudioAcademy\Game\Model\Harbor\PirateHarbor;
use BinaryStudioAcademy\Game\Model\Ship\ShipFactory;

class PirateHarborFactory implements HarborFactory
{

    function createHarbor(int $number, string $name): Harbor
    {
        return new PirateHarbor($number, $name, ShipFactory::createPlayerShip());
    }
}