<?php


namespace BinaryStudioAcademy\Game\Model\Harbor;


use BinaryStudioAcademy\Game\Contracts\Model\Harbor;
use BinaryStudioAcademy\Game\Contracts\Model\Ship;
use BinaryStudioAcademy\Game\Contracts\Model\ShopItem;
use BinaryStudioAcademy\Game\Exception\LogicException;
use BinaryStudioAcademy\Game\Model\Ship\State\HomeState;
use BinaryStudioAcademy\Game\Model\Ship\State\SailState;
use BinaryStudioAcademy\Game\Model\Ship\State\State;
use BinaryStudioAcademy\Game\Model\Shop\SaleStrategy\SaleStrategy;

abstract class AbstractHarbor implements Harbor
{
    private $number;
    private $name;
    private $ship;
    private $neighbors = [];
    private $saleStrategy;

    public function __construct(int $number, string $name, Ship $ship)
    {
        $this->number = $number;
        $this->name = $name;
        $this->ship = $ship;
        $this->ship->setCurrentHarbor($this);
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShip(): Ship
    {
        return $this->ship;
    }

    /**
     * @throws LogicException
     */
    public function getNeighbor(string $direction): Harbor
    {
        if (!isset($this->neighbors[$direction])){
            throw new LogicException('Harbor not found in this direction');
        }

        return $this->neighbors[$direction];
    }

    public function setNeighbor(string $direction, Harbor $harbor): Harbor
    {
        $this->neighbors[$direction] = $harbor;
        return $this;
    }

    public function getDescription(): string
    {
        return "Harbor {$this->getNumber()}: {$this->getName()}." . PHP_EOL;
    }

    public function onShipVisit(): string
    {
        return $this->resetShip();
    }

    public function getStateForVisitorShip(Ship $ship): State
    {
        if ($ship == $this->getShip() ){
            $state = new HomeState();
        }else{
            $state = new SailState();
        }
        $state->setShip($ship);
        return $state;
    }


    public function setSaleStrategy(SaleStrategy $saleStrategy): Harbor
    {
        $this->saleStrategy = $saleStrategy;

        return $this;
    }

    /**
     * @throws LogicException
     */
    public function sale(string $itemName): ?ShopItem {
        if (!$this->saleStrategy){
            throw new LogicException('Sale strategy is not defined');
        }
        return $this->saleStrategy->sale($itemName);
    }

}