<?php


namespace BinaryStudioAcademy\Game\Model\Harbor;


class PirateHarbor extends AbstractHarbor
{

    function resetShip(): string
    {
        if ($this->getShip()->repairHealth())
        {
            return sprintf('Your health is repared to %s.' . PHP_EOL, $this->getShip()->getHealth());
        }
        return '';
    }
}