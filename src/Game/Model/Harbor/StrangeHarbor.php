<?php


namespace BinaryStudioAcademy\Game\Model\Harbor;


class StrangeHarbor extends AbstractHarbor
{
    public function resetShip(): string
    {
        $this->getShip()->repairHealth();
        $this->getShip()->resetHold();
        return '';
    }
}