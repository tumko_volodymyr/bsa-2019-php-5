<?php


namespace BinaryStudioAcademy\Game\Model\Commands;


use BinaryStudioAcademy\Game\Contracts\Model\Command;

abstract class AbstractCommand implements Command
{

    public function getAlias(): string
    {
        if (null === static::ALIAS){
            throw new \LogicException(sprintf('Command should have an alias'));
        }
        return static::ALIAS;
    }

    public function getDescription(): string
    {
        if (null === static::DESCRIPTION){
            throw new \LogicException(sprintf('Command should have a description'));
        }
        return static::DESCRIPTION;
    }

    public function getArguments(): string
    {
        return '';
    }
}