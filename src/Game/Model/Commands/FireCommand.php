<?php


namespace BinaryStudioAcademy\Game\Model\Commands;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;

class FireCommand extends AbstractCommand
{
    const ALIAS = 'fire';
    const DESCRIPTION = 'attacks enemy\'s ship';

    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function execute(...$params): string
    {
        return $this->ship->fire();
    }
}