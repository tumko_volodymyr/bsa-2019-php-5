<?php


namespace BinaryStudioAcademy\Game\Model\Commands;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;

class WhereAreMeCommand extends AbstractCommand
{
    const ALIAS = 'whereami';
    const DESCRIPTION = 'shows current harbor';

    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function execute(...$params): string
    {
        return $this->ship->whereAreMe();
    }
}