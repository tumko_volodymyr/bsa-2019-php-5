<?php


namespace BinaryStudioAcademy\Game\Model\Commands;


class ExitCommand extends AbstractCommand
{
    const ALIAS = 'exit';
    const DESCRIPTION = 'finishes the game';
    const RESPONSE = 'Good bay';

    public function execute(...$params): string
    {
        return static::RESPONSE;
    }
}