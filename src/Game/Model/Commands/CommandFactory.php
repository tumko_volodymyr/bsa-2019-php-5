<?php


namespace BinaryStudioAcademy\Game\Model\Commands;


use BinaryStudioAcademy\Game\Contracts\Model\Command;
use BinaryStudioAcademy\Game\Contracts\Model\Game;
use BinaryStudioAcademy\Game\Contracts\Model\Ship;

class CommandFactory
{
    public static function createHelpCommand (Game $game): Command
    {
        return new HelpCommand($game);
    }

    public static function createStatsCommand (Ship $ship): Command
    {
        return new StatsCommand($ship);
    }

    public static function createSailCommand (Ship $ship): Command
    {
        return new SailCommand($ship);
    }

    public static function createFireCommand (Ship $ship): Command
    {
        return new FireCommand($ship);
    }

    public static function createAboardCommand (Ship $ship): Command
    {
        return new AboardCommand($ship);
    }

    public static function createBuyCommand (Ship $ship): Command
    {
        return new BuyCommand($ship);
    }

    public static function createDrinkCommand (Ship $ship): Command
    {
        return new DrinkCommand($ship);
    }

    public static function createWhereamiCommand (Ship $ship): Command
    {
        return new WhereAreMeCommand($ship);
    }

    public static function createExitCommand (): Command
    {
        return new ExitCommand();
    }

}