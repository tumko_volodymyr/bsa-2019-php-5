<?php


namespace BinaryStudioAcademy\Game\Model\Commands;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;

class AboardCommand extends AbstractCommand
{
    const ALIAS = 'aboard';
    const DESCRIPTION = 'collect loot from the ship';

    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function execute(...$params): string
    {
        return $this->ship->aboard();
    }

}