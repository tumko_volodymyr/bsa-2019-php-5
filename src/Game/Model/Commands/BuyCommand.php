<?php


namespace BinaryStudioAcademy\Game\Model\Commands;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;
use BinaryStudioAcademy\Game\Exception\InvalidArgumentException;

class BuyCommand extends AbstractCommand
{
    const ALIAS = 'buy';
    const DESCRIPTION = 'buys skill or rum: 1 chest of gold - 1 item';

    const ITEM_STRENGTH = 'strength';
    const ITEM_ARMOUR = 'armour';
    const ITEM_LUCK= 'luck';
    const ITEM_RUM = 'rum';

    const ITEMS = [
        self::ITEM_STRENGTH,
        self::ITEM_ARMOUR,
        self::ITEM_LUCK,
        self::ITEM_RUM,
    ];

    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function getArguments(): string
    {
        return sprintf('<%s> ', implode('|', self::ITEMS));
    }

    /**
     * @throws InvalidArgumentException
     */
    public function execute(...$params): string
    {
        list($itemName) = $params;
        if (!in_array($itemName, self::ITEMS)){
            throw new InvalidArgumentException(
                sprintf(
                    'Goods item \'%s\' incorrect, choose from: %s',
                    $itemName,
                    implode(', ', self::ITEMS)
                )
            );
        }
        return $this->ship->buy($itemName);
    }
}