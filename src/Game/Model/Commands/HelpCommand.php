<?php


namespace BinaryStudioAcademy\Game\Model\Commands;


use BinaryStudioAcademy\Game\Contracts\Model\Game;

final  class HelpCommand extends AbstractCommand
{
    const ALIAS = 'help';
    const DESCRIPTION = 'shows this list of commands';

    private $game;

    public function __construct(Game $game)
    {
        $this->game = $game;
    }

    public function execute(...$params): string
    {
        $result = 'List of commands:' . PHP_EOL;
        foreach ($this->game->getCommands() as $command){
            $result .= sprintf('%s %s- %s'.PHP_EOL,
                $command->getAlias(),
                $command->getArguments(),
                $command->getDescription()
            );
        }
        return $result;
    }
}