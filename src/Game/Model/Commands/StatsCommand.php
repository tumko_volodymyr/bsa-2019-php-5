<?php


namespace BinaryStudioAcademy\Game\Model\Commands;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;

class StatsCommand extends AbstractCommand
{
    const ALIAS = 'stats';
    const DESCRIPTION = 'shows stats of ship';

    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function execute(...$params): string
    {
        return $this->ship->getStats();
    }
}