<?php


namespace BinaryStudioAcademy\Game\Model\Commands;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;
use BinaryStudioAcademy\Game\Exception\InvalidArgumentException;
use BinaryStudioAcademy\Game\Exception\LogicException;

class SailCommand extends AbstractCommand
{
    const ALIAS = 'set-sail';
    const DESCRIPTION = 'moves in given direction';

    const DIRECTION_EAST = 'east';
    const DIRECTION_WEST = 'west';
    const DIRECTION_NORTH = 'north';
    const DIRECTION_SOUTH = 'south';

    const DIRECTIONS = [
        self::DIRECTION_EAST,
        self::DIRECTION_WEST,
        self::DIRECTION_NORTH,
        self::DIRECTION_SOUTH,
    ];

    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function getArguments(): string
    {
        return sprintf('<%s> ', implode('|', self::DIRECTIONS));
    }


    /**
     * @throws InvalidArgumentException
     * @throws LogicException
     */
    public function execute(...$params): string
    {
        list($direction) = $params;
        if (!in_array($direction, self::DIRECTIONS)){
            throw new InvalidArgumentException(
                sprintf(
                    'Direction \'%s\' incorrect, choose from: %s',
                    $direction,
                    implode(', ', self::DIRECTIONS)
                )
            );
        }
        return $this->ship->sail($direction);
    }
}