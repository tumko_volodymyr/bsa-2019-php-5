<?php


namespace BinaryStudioAcademy\Game\Model\Commands;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;

class DrinkCommand extends AbstractCommand
{
    const ALIAS = 'drink';
    const DESCRIPTION = 'your captain drinks 1 bottle of rum and fill 30 points of health';

    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function execute(...$params): string
    {
        return $this->ship->drink();
    }
}