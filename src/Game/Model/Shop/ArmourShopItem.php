<?php


namespace BinaryStudioAcademy\Game\Model\Shop;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;
use BinaryStudioAcademy\Game\Model\Commands\BuyCommand;

class ArmourShopItem extends AbstractSkillShopItem
{

    protected function updateSkill(Ship $ship): void
    {
        $ship->incrementArmour();
    }

    protected function getSkillName(): string
    {
        return BuyCommand::ITEM_ARMOUR;
    }

    protected function getSkillValue(Ship $ship): string
    {
        return (string) $ship->getArmour();
    }
}