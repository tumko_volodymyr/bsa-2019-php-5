<?php


namespace BinaryStudioAcademy\Game\Model\Shop;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;
use BinaryStudioAcademy\Game\Model\Commands\BuyCommand;

class StrengthShopItem extends AbstractSkillShopItem
{

    protected function updateSkill(Ship $ship): void
    {
        $ship->incrementStrength();
    }

    protected function getSkillName(): string
    {
        return BuyCommand::ITEM_STRENGTH;
    }

    protected function getSkillValue(Ship $ship): string
    {
        return (string) $ship->getStrength();
    }
}