<?php


namespace BinaryStudioAcademy\Game\Model\Shop;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;
use BinaryStudioAcademy\Game\Contracts\Model\ShopItem;

abstract class AbstractSkillShopItem implements ShopItem
{

    public function saveOnShip(Ship $ship): string
    {
        $this->updateSkill($ship);
        return  sprintf("Your %s is %s." . PHP_EOL, $this->getSkillName(), $this->getSkillValue($ship));
    }

    abstract protected function updateSkill(Ship $ship): void ;
    abstract protected function getSkillName(): string ;
    abstract protected function getSkillValue(Ship $ship): string ;
}