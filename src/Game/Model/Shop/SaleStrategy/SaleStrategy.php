<?php


namespace BinaryStudioAcademy\Game\Model\Shop\SaleStrategy;


use BinaryStudioAcademy\Game\Contracts\Model\ShopItem;

interface SaleStrategy
{
    public function sale(string $itemName): ?ShopItem ;
}