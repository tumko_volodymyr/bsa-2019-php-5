<?php


namespace BinaryStudioAcademy\Game\Model\Shop\SaleStrategy;


use BinaryStudioAcademy\Game\Contracts\Model\ShopItem;

class NoGoodsSaleStrategy implements SaleStrategy
{

    public function sale(string $itemName): ?ShopItem
    {
        return null;
    }
}