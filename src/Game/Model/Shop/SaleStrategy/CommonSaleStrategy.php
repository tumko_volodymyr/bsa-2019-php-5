<?php


namespace BinaryStudioAcademy\Game\Model\Shop\SaleStrategy;


use BinaryStudioAcademy\Game\Contracts\Model\ShopItem;
use BinaryStudioAcademy\Game\Model\Commands\BuyCommand;
use BinaryStudioAcademy\Game\Model\Shop\ArmourShopItem;
use BinaryStudioAcademy\Game\Model\Shop\LuckShopItem;
use BinaryStudioAcademy\Game\Model\Shop\RumShopItem;
use BinaryStudioAcademy\Game\Model\Shop\StrengthShopItem;

class CommonSaleStrategy implements SaleStrategy
{
    public function sale(string $itemName): ?ShopItem
    {
        if (BuyCommand::ITEM_STRENGTH === $itemName){
            return new StrengthShopItem();
        }

        if (BuyCommand::ITEM_ARMOUR === $itemName){
            return new ArmourShopItem();
        }

        if (BuyCommand::ITEM_LUCK === $itemName){
            return new LuckShopItem();
        }

        if (BuyCommand::ITEM_RUM === $itemName){
            return new RumShopItem();
        }

        return null;
    }
}