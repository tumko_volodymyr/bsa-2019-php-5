<?php


namespace BinaryStudioAcademy\Game\Model\Shop;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;
use BinaryStudioAcademy\Game\Contracts\Model\ShopItem;
use BinaryStudioAcademy\Game\Model\Ship\HoldItem\Rum;

class RumShopItem implements ShopItem
{
    public function saveOnShip(Ship $ship): string
    {
        $ship->addHoldItem(new Rum());
        return "Your hold contains {$ship->getRumCount()} bottle(s) of rum." . PHP_EOL;
    }
}