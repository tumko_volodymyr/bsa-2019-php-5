<?php


namespace BinaryStudioAcademy\Game\Model\Shop;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;
use BinaryStudioAcademy\Game\Model\Commands\BuyCommand;

class LuckShopItem extends AbstractSkillShopItem
{

    protected function updateSkill(Ship $ship): void
    {
        $ship->incrementLuck();
    }

    protected function getSkillName(): string
    {
        return BuyCommand::ITEM_LUCK;
    }

    protected function getSkillValue(Ship $ship): string
    {
        return (string) $ship->getLuck();
    }
}