<?php


namespace BinaryStudioAcademy\Game\Model\Ship;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;

class ShipFactory
{
    public static function createPlayerShip(): Ship
    {
        return new PirateShip('Player Ship', 4, 4,4, PirateShip::INIT_HEALTH);
    }

    public static function createSchoonerShip(): Ship
    {
        return new SchoonerShip('Royal Patrool Schooner', 4, 4,4, SchoonerShip::INIT_HEALTH);
    }

    public static function createBattleShip(): Ship
    {
        return new BattleShip('Royal Battle Ship', 8, 8,7, BattleShip::INIT_HEALTH);
    }

    public static function createRoyalShip(): Ship
    {
        return new RoyalShip('HMS Royal Sovereign', 10, 10,10, RoyalShip::INIT_HEALTH);
    }
}