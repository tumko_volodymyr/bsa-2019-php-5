<?php


namespace BinaryStudioAcademy\Game\Model\Ship;


use BinaryStudioAcademy\Game\Model\Ship\HoldItem\Gold;
use BinaryStudioAcademy\Game\Model\Ship\HoldItem\Rum;

class RoyalShip extends AbstractShip
{

    const INIT_HEALTH = 100;

    public function getType(): string
    {
        return static::TYPE_ROYAL;
    }

    protected function setInitHold(): void
    {
        $this->hold[] = [ new Gold(), new Gold(), new Rum() ];
    }

}