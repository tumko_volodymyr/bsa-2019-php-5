<?php


namespace BinaryStudioAcademy\Game\Model\Ship;


use BinaryStudioAcademy\Game\Contracts\Helpers\Math as IMath;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Model\Harbor;
use BinaryStudioAcademy\Game\Contracts\Model\HoldItem;
use BinaryStudioAcademy\Game\Contracts\Model\Ship;
use BinaryStudioAcademy\Game\Exception\InvalidArgumentException;
use BinaryStudioAcademy\Game\Exception\LogicException;
use BinaryStudioAcademy\Game\Exception\ShipHoldIsFullException;
use BinaryStudioAcademy\Game\Game;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Helpers\Stats;
use BinaryStudioAcademy\Game\Model\Ship\HoldItem\Gold;
use BinaryStudioAcademy\Game\Model\Ship\HoldItem\Rum;
use BinaryStudioAcademy\Game\Model\Ship\State\State;

abstract class AbstractShip implements Ship
{

    private $name;
    private $strength;
    private $armour;
    private $luck;
    private $health;
    protected $hold = [];
    private $currentHarbor;
    private $initialHarbor;
    private $state;
    private $random;

    public function __construct(string $name, int  $strength, int $armour, int $luck, int $health)
    {
        $this->name = $name;
        $this->strength = $strength;
        $this->armour = $armour;
        $this->luck = $luck;
        $this->health = $health;
        $this->setInitHold();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getArmour(): int
    {
        return $this->armour;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getHold(): array
    {
        return $this->hold;
    }

    private function getHoldDescription()
    {
        $hold = array_map( function ($element){
            return $element->getView();
        }, array_values($this->getHold()));

        if (count($hold) === 0){
            return '[ _ _ _ ]';
        }

        $hold[] = '_';
        $hold[] = '_';

        list($firstItem, $secondItem, $thirdItem) = $hold;

        return sprintf('[ %s %s %s ]', $firstItem??'_', $secondItem??'_', $thirdItem??'_');
    }

    public function getStats(): string
    {
        return 'Ship stats:' . PHP_EOL
            . 'strength: ' . $this->getStrength() . PHP_EOL
            . 'armour: ' . $this->getArmour() . PHP_EOL
            . 'luck: ' . $this->getLuck()  . PHP_EOL
            . 'health: ' . $this->getHealth()   . PHP_EOL
            . 'hold: ' . $this->getHoldDescription() . PHP_EOL;
    }

    public function getCurrentHarbor(): Harbor
    {
        return $this->currentHarbor;
    }

    public function setCurrentHarbor($currentHarbor): Ship
    {
        if (null === $this->currentHarbor){
            $this->initialHarbor = $currentHarbor;
        }
        $this->currentHarbor = $currentHarbor;

        return $this;
    }

    public function repairHealth(): bool
    {
        if (null === static::INIT_HEALTH){
            throw new \LogicException(sprintf('Ship should have init health'));
        }

        if ($this->getHealth() < static::INIT_HEALTH){
            $this->health = static::INIT_HEALTH;
            return true;
        }
        return false;
    }

    public function sail(string $direction): string
    {
        try {
            $newHarbor = $this->getCurrentHarbor()->getNeighbor($direction);
        } catch (LogicException $e) {
            return $e->getMessage();
        }
        $this->setCurrentHarbor($newHarbor);
        $this->setState($newHarbor->getStateForVisitorShip($this));
        return $newHarbor->getDescription().$newHarbor->onShipVisit().$this->exploreHarbor();
    }

    public function whereAreMe(): string
    {
        return "Harbor {$this->getCurrentHarbor()->getNumber()}: {$this->getCurrentHarbor()->getName()}" . PHP_EOL;
    }

    public function isAlive(): bool
    {
        return $this->getHealth() > 0;
    }

    public function fire(): string
    {
        return $this->getState()->fire();
    }

    public function aboard(): string
    {
        return $this->getState()->aboard();
    }

    public function drink(): string
    {
        $rum = $this->takeRumFromHold();
        if (null == $rum){
            return "You have not rum in hold" . PHP_EOL;
        }
        $this->increaseHealth(Game::RUM_HEALTH);
        return "You\'ve drunk a rum and your health filled to {$this->getHealth()}" . PHP_EOL;
    }

    public function buy(string $itemName): string
    {
        if (!$this->takeGoldFromHold()){
            return 'You have not gold for buy';
        }

        $shopItem = $this->getCurrentHarbor()->sale($itemName);

        if (null === $shopItem){
            return 'Current harbor has not goods for sale.' . PHP_EOL;
        }

        return "You\'ve bought a {$itemName}. " . $shopItem->saveOnShip($this) . PHP_EOL;;
    }

    private function exploreHarbor(): string
    {
        return $this->getState()->exploreHarbor();
    }

    private function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): Ship
    {
        $this->state = $state;
        return $this;
    }

    public function setRandom(Random $random): Ship
    {
        $this->random = $random;
        return $this;
    }

    private function getMath(): IMath
    {
        return new Math();
    }

    private function getShotLuck(): bool
    {
        return $this->getMath()->luck($this->random, $this->getLuck());
    }

    private function getShotDamageOnShip(Ship $ship): int
    {
        if (!$this->getShotLuck()){
            return 0;
        }

        return $this->getMath()->damage($this->getStrength(), $ship->getArmour());
    }

    public function takeShot(Ship $ship): int
    {
        $damage = $this->getShotDamageOnShip($ship);
        $ship->onShot($damage);
        return $damage;
    }

    public function onShot(int  $damage): void
    {
        $this->subtractHealth($damage);
    }

    /**
     * @throws InvalidArgumentException
     */
    private function subtractHealth(int $damage): void
    {
        if ($damage < 0){
            throw new InvalidArgumentException('Damage value should be positive integer');
        }

        if ($damage > $this->getHealth()){
            $this->health = 0;
        }else{
            $this->health -= $damage;
        }
    }

    abstract protected function setInitHold(): void ;

    /**
     * @throws ShipHoldIsFullException
     */
    public function addHoldItem(HoldItem $item): void {
        if (count($this->hold) === 3){
            throw new ShipHoldIsFullException();
        }
        $this->hold[] = $item;
    }

    public function takeOneItemFromHold(): HoldItem {
        return array_shift($this->hold);
    }

    public function resetHold(): void
    {
        $this->setInitHold();
    }

    public function retry(): void
    {
        $this->strength--;
        $this->armour--;
        $this->luck--;
        $this->health--;
        $this->resetHold();
        $this->repairHealth();
        $this->currentHarbor = $this->initialHarbor;
    }

    public function incrementStrength(): void
    {
        if ($this->strength < Stats::MAX_STRENGTH){
            $this->strength++;
        }
    }

    public function incrementArmour(): void
    {
        if ($this->armour < Stats::MAX_ARMOUR){
            $this->armour++;
        }
    }

    public function incrementLuck(): void
    {
        if ($this->luck < Stats::MAX_LUCK){
            $this->luck++;
        }
    }

    public function increaseHealth(int $volume): void
    {
        if ($this->health === Stats::MAX_HEALTH){
            return;
        }

        if ($this->health + $volume > Stats::MAX_HEALTH){
            $this->health = Stats::MAX_HEALTH;
            return;
        }

        $this->health += $volume;
    }

    private function takeGoldFromHold(): ?Gold
    {
        return $this->takeItemFromHoldByType(HoldItem::TYPE_GOLD);
    }

    private function takeRumFromHold(): ?Rum
    {
        return $this->takeItemFromHoldByType(HoldItem::TYPE_RUM);
    }

    private function takeItemFromHoldByType (string  $type)
    {
        foreach ($this->hold as $key => $item){
            /** @var HoldItem $item */
            if ( $type === $item->getType()){
                unset($this->hold[$key]);
                return $item;
            }
        }
        return null;
    }

    public function getRumCount(): int
    {
        return count(array_filter($this->getHold(), function ($element){
            return $element->getType() === HoldItem::TYPE_RUM;
        }));
    }


}