<?php


namespace BinaryStudioAcademy\Game\Model\Ship;


use BinaryStudioAcademy\Game\Model\Ship\HoldItem\Rum;

class BattleShip extends AbstractShip
{

    const INIT_HEALTH = 80;

    public function getType(): string
    {
        return static::TYPE_BATTLE;
    }

    protected function setInitHold(): void
    {
        $this->hold = [ new Rum() ];
    }
}