<?php


namespace BinaryStudioAcademy\Game\Model\Ship;


class PirateShip extends AbstractShip
{
    const INIT_HEALTH = 60;

    public function getType(): string
    {
        return static::TYPE_PLAYER;
    }

    protected function setInitHold(): void
    {
        $this->hold = [];
    }
}