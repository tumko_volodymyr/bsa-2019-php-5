<?php


namespace BinaryStudioAcademy\Game\Model\Ship\State;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;

interface State
{
    public function fire(): string ;
    public function aboard(): string ;
    public function exploreHarbor(): string ;
    public function setShip(Ship $ship): void ;
}