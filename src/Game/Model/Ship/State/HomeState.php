<?php


namespace BinaryStudioAcademy\Game\Model\Ship\State;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;

class HomeState implements State
{
    /**
     * @var Ship
     */
    private $ship;

    public function fire(): string
    {
        return 'There is no ship to fight';
    }

    public function aboard(): string
    {
        return 'There is no ship to aboard';
    }

    public function exploreHarbor(): string
    {
        return '';
    }

    public function setShip(Ship $ship): void
    {
        $this->ship = $ship;
    }
}