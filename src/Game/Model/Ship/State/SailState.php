<?php


namespace BinaryStudioAcademy\Game\Model\Ship\State;


use BinaryStudioAcademy\Game\Contracts\Model\Ship;
use BinaryStudioAcademy\Game\Exception\ShipHoldIsFullException;
use BinaryStudioAcademy\Game\Game;

class SailState implements State
{
    /**
     * @var Ship
     */
    private $ship;

    public function fire(): string
    {
        $enemyShip = $this->getEnemyShip();
        $enemyDamage = $this->ship->takeShot($enemyShip);
        if (!$enemyShip->isAlive()){
            if ($enemyShip->getType() === Ship::TYPE_ROYAL ){
                return Game::CONGRATULATIONS;
            }
            return "{$enemyShip->getName()} on fire. Take it to the boarding." . PHP_EOL;
        }

        $playerDamage = $enemyShip->takeShot($this->ship);
        if (!$this->ship->isAlive()){
            $this->ship->retry();

            return "Your ship has been sunk." . PHP_EOL
                . "You restored in the Pirate Harbor." . PHP_EOL
                . "You lost all your possessions and 1 of each stats." . PHP_EOL;
        }

        $enemyShipIsAlive = $enemyShip->isAlive()?'true':'false';
        $shipName = $enemyShip->getName();
        return "{$shipName} has damaged on: {$enemyDamage} points." . PHP_EOL
            . "health: {$enemyShip->getHealth()}" . PHP_EOL
            . "{$shipName} damaged your ship on: {$playerDamage} points." . PHP_EOL
            . "health: {$this->ship->getHealth()}" . PHP_EOL
            . "isAlive: {$enemyShipIsAlive}" . PHP_EOL;
    }

    public function aboard(): string
    {
        $enemyShip = $this->getEnemyShip();
        if ($enemyShip->isAlive()){
            return 'You cannot board this ship, since it has not yet sunk';
        }

        if (!($count = count($enemyShip->getHold()))){
            return 'Ship has no more goods';
        }

        $goods = '';
        do {
            $value = $enemyShip->takeOneItemFromHold();
            $goods .= "You got {$value->getView()}." . PHP_EOL;
            try{
                $this->ship->addHoldItem($value);
            }catch (ShipHoldIsFullException $exception){
                throw $exception;
                return $goods.
                    'For more items ship has not place' . PHP_EOL;
            }
            $count--;
        }while($count > 0);
        return $goods;
    }

    public function exploreHarbor(): string
    {
        $harborShip = $this->getEnemyShip();
        return "You see {$harborShip->getName()}: " . PHP_EOL
            . 'strength: ' . $harborShip->getStrength() . PHP_EOL
            . 'armour: ' . $harborShip->getArmour() . PHP_EOL
            . 'luck: ' . $harborShip->getLuck()  . PHP_EOL
            . 'health: ' . $harborShip->getHealth()   . PHP_EOL;
    }

    public function setShip(Ship $ship): void
    {
        $this->ship = $ship;
    }

    private function getEnemyShip(): Ship
    {
        return $this->ship->getCurrentHarbor()->getShip();
    }
}