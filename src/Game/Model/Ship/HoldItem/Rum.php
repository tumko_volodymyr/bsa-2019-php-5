<?php


namespace BinaryStudioAcademy\Game\Model\Ship\HoldItem;


use BinaryStudioAcademy\Game\Contracts\Model\HoldItem;

class Rum implements HoldItem
{

    public function getView(): string
    {
        return static::VIEW_RUM;
    }

    public function getType(): string
    {
        return static::TYPE_RUM;
    }
}