<?php


namespace BinaryStudioAcademy\Game\Model\Ship\HoldItem;


use BinaryStudioAcademy\Game\Contracts\Model\HoldItem;

class Gold implements HoldItem
{

    public function getView(): string
    {
        return static::VIEW_GOLD;
    }

    public function getType(): string
    {
        return static::TYPE_GOLD;
    }
}