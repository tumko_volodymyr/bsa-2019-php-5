<?php


namespace BinaryStudioAcademy\Game\Model\Ship;


use BinaryStudioAcademy\Game\Model\Ship\HoldItem\Gold;

class SchoonerShip extends AbstractShip
{

    const INIT_HEALTH = 50;

    public function getType(): string
    {
        return static::TYPE_SCHOONER;
    }

    protected function setInitHold(): void
    {
        $this->hold = [ new Gold() ];
    }
}