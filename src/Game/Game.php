<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Model\Command;
use BinaryStudioAcademy\Game\Contracts\Model\Harbor;
use BinaryStudioAcademy\Game\Contracts\Model\Ship;
use BinaryStudioAcademy\Game\Exception\InvalidArgumentException;
use BinaryStudioAcademy\Game\Exception\LogicException;
use BinaryStudioAcademy\Game\Model\Commands\CommandFactory;
use BinaryStudioAcademy\Game\Model\Commands\ExitCommand;
use BinaryStudioAcademy\Game\Model\Harbor\Factory\BattleHarborFactory;
use BinaryStudioAcademy\Game\Model\Harbor\Factory\PirateHarborFactory;
use BinaryStudioAcademy\Game\Model\Harbor\Factory\RoyalHarborFactory;
use BinaryStudioAcademy\Game\Model\Harbor\Factory\SchoonerHarborFactory;
use BinaryStudioAcademy\Game\Model\Shop\SaleStrategy\CommonSaleStrategy;
use BinaryStudioAcademy\Game\Model\Shop\SaleStrategy\NoGoodsSaleStrategy;

class Game implements Contracts\Model\Game
{
    const RUM_HEALTH = 30;
    const CONGRATULATIONS = '🎉🎉🎉Congratulations🎉🎉🎉' . PHP_EOL
    . '💰💰💰 All gold and rum of Great Britain belong to you! 🍾🍾🍾';
    private $random;

    private $commands = [];
    private $harbors = [];

    public function __construct(Random $random)
    {
        $this->random = $random;
        $pirateHarborFactory = new PirateHarborFactory;
        $pirateHarbor = $pirateHarborFactory->createHarbor(1, 'Pirates Harbor');
        $pirateHarbor->setSaleStrategy(new CommonSaleStrategy());
        $this->addHarbor($pirateHarbor);
        $playerShip = $pirateHarbor->getShip();
        $playerShip->setState($pirateHarbor->getStateForVisitorShip($playerShip));
        $this->initCommands($playerShip);
        $this->initRestHarbors();
        $this->setUpRandomForShips($random);
    }

    public function start(Reader $reader, Writer $writer): void
    {
        $writer->writeln('You are welcome.');
        $writer->writeln('This is a game "Battle Ship".');
        $writer->writeln('To see all available command type "help".');
        $writer->writeln('Press enter to start... ');
        $input = trim($reader->read());
        $writer->writeln('Adventure has begun. Wish you good luck!');
        $writer->writeln('You are in your harbor.');
        do{
            $writer->writeln('Type your command');
            $input = trim($reader->read());
            try{
                $processingResult = $this->processCommand($input);
                $writer->writeln($processingResult);
            }catch (InvalidArgumentException | LogicException $exception){
                $writer->writeln($exception->getMessage());
            }
        }while($processingResult !== ExitCommand::RESPONSE && $processingResult !== self::CONGRATULATIONS);
    }

    public function run(Reader $reader, Writer $writer): void
    {
        $input = trim($reader->read());
        $writer->writeln($this->processCommand($input));
    }

    public function getCommands(): array
    {
        return $this->commands;
    }

    public function addCommand(Command $command): Contracts\Model\Game
    {
        $this->commands[$command->getAlias()] = $command;
        return $this;
    }

    private function processCommand (string $commandStr): string {
        $arguments = explode(' ', $commandStr);
        $command = array_shift($arguments);

        try{
            if (!array_key_exists($command, $this->commands)){
                throw new InvalidArgumentException(sprintf("Command '%s' not found", $command));
            }
            return $this->commands[$command]->execute(...$arguments);
        }catch (InvalidArgumentException $exception){
            return $exception->getMessage();
        }
    }

    private function initCommands(Ship $ship): void
    {
        $this->addCommand(CommandFactory::createHelpCommand($this))
            ->addCommand(CommandFactory::createStatsCommand($ship))
            ->addCommand(CommandFactory::createSailCommand($ship))
            ->addCommand(CommandFactory::createFireCommand($ship))
            ->addCommand(CommandFactory::createAboardCommand($ship))
            ->addCommand(CommandFactory::createBuyCommand($ship))
            ->addCommand(CommandFactory::createDrinkCommand($ship))
            ->addCommand(CommandFactory::createWhereamiCommand($ship))
            ->addCommand(CommandFactory::createExitCommand());
    }

    private function initRestHarbors(): void
    {
        $schoonerHarborFactory = new SchoonerHarborFactory();
        $battleHarborFactory = new BattleHarborFactory();
        $royalHarborFactory = new RoyalHarborFactory();
        $noGoodsStrategy = new NoGoodsSaleStrategy();
        $this->addHarbor($schoonerHarborFactory->createHarbor(2, 'Southhampton')->setSaleStrategy($noGoodsStrategy))
            ->addHarbor($schoonerHarborFactory->createHarbor(3, 'Fishguard')->setSaleStrategy($noGoodsStrategy))
            ->addHarbor($schoonerHarborFactory->createHarbor(4, 'Salt End')->setSaleStrategy($noGoodsStrategy))
            ->addHarbor($schoonerHarborFactory->createHarbor(5, 'Isle of Grain')->setSaleStrategy($noGoodsStrategy))
            ->addHarbor($battleHarborFactory->createHarbor(6, 'Grays')->setSaleStrategy($noGoodsStrategy))
            ->addHarbor($battleHarborFactory->createHarbor(7, 'Felixstowe')->setSaleStrategy($noGoodsStrategy))
            ->addHarbor($royalHarborFactory->createHarbor(8, 'London Docks')->setSaleStrategy($noGoodsStrategy));

        $this->setUpHarborsNeighbors();
    }

    private function setUpHarborsNeighbors()
    {
        $neighborsMap = [
            1 => [
                'west' => 3,
                'north' => 4,
                'south' => 2,
            ],
            2 => [
                'east' => 7,
                'west' => 3,
                'north' => 1,
            ],
            3 => [
                'east' => 1,
                'north' => 4,
                'south' => 2,
            ],
            4 => [
                'east' => 5,
                'west' => 3,
                'south' => 1,
            ],
            5 => [
                'east' => 6,
                'west' => 4,
                'south' => 7,
            ],
            6 => [
                'west' => 5,
                'south' => 8,
            ],
            7 => [
                'east' => 8,
                'west' => 2,
                'north' => 5,
            ],
            8 => [
                'west' => 7,
                'north' => 6,
            ]
        ];

        foreach ($neighborsMap as $key => $values){
            foreach ($values as $direction => $id){
                $this->harbors[$key]->setNeighbor($direction, $this->harbors[$id]);
            }
        }
    }


    private function setUpRandomForShips(Random $random)
    {
        /** @var Harbor $harbor */
        foreach ($this->harbors as $harbor){
            $harbor->getShip()->setRandom($random);
        }
    }

    private function addHarbor (Harbor $harbor): self
    {
        $this->harbors[$harbor->getNumber()] = $harbor;
        return $this;
    }
}
