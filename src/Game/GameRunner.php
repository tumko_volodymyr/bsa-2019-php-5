<?php


namespace BinaryStudioAcademy\Game;


use BinaryStudioAcademy\Game\Helpers\Random;
use BinaryStudioAcademy\Game\Io\CliReader;
use BinaryStudioAcademy\Game\Io\CliWriter;

class GameRunner
{
    public function __construct()
    {
        $random = new Random();
        $reader = new CliReader();
        $writer = new CliWriter();
        $game = new Game($random);
        $game->start($reader, $writer);
    }
}