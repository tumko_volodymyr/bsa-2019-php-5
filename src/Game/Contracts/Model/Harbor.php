<?php


namespace BinaryStudioAcademy\Game\Contracts\Model;


use BinaryStudioAcademy\Game\Exception\LogicException;
use BinaryStudioAcademy\Game\Model\Ship\State\State;
use BinaryStudioAcademy\Game\Model\Shop\SaleStrategy\SaleStrategy;

interface Harbor extends SaleStrategy
{
    public function getNumber(): int ;
    public function getName(): string ;
    /**
     * @throws LogicException
     */
    public function getNeighbor(string $direction): Harbor ;
    public function setNeighbor(string $direction, Harbor $harbor): Harbor ;
    public function getShip(): Ship ;
    public function getDescription(): string ;
    public function onShipVisit(): string ;
    public function getStateForVisitorShip(Ship $ship): State ;
    public function resetShip(): string ;
    public function setSaleStrategy(SaleStrategy $saleStrategy): Harbor ;

}