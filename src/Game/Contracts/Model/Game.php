<?php


namespace BinaryStudioAcademy\Game\Contracts\Model;


use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

interface Game
{
    public function __construct(Random $random);

    public function start(Reader $reader, Writer $writer): void ;
    public function run(Reader $reader, Writer $writer): void ;

    public function getCommands(): array ;
    public function addCommand(Command $command): self ;
}