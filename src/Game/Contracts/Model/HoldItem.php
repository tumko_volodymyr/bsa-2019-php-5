<?php


namespace BinaryStudioAcademy\Game\Contracts\Model;


interface HoldItem
{
    const TYPE_RUM = 'rum';
    const TYPE_GOLD = 'gold';
    const VIEW_RUM = '🍾';
    const VIEW_GOLD = '💰';

    public function getView(): string ;
    public function getType(): string ;
}