<?php


namespace BinaryStudioAcademy\Game\Contracts\Model;


use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Model\Ship\State\State;

interface Ship
{

    const INIT_HEALTH = null;
    const TYPE_PLAYER = 'player';
    const TYPE_SCHOONER = 'schooner';
    const TYPE_BATTLE = 'battle';
    const TYPE_ROYAL = 'royal';

    public function getName(): string ;
    public function getType(): string ;
    public function getStrength(): int ;
    public function getArmour(): int ;
    public function getLuck(): int ;
    public function getHealth(): int ;
    public function getRumCount(): int ;
    public function incrementStrength(): void ;
    public function incrementArmour(): void ;
    public function incrementLuck(): void ;
    public function increaseHealth(int $volume): void ;
    public function getHold(): array ;
    public function resetHold(): void ;
    public function addHoldItem(HoldItem $item): void ;
    public function takeOneItemFromHold(): HoldItem ;

    public function fire(): string ;
    public function sail(string $direction): string ;
    public function getStats(): string ;
    public function aboard(): string ;
    public function drink(): string ;
    public function whereAreMe(): string ;
    public function buy(string $itemName): string ;
    public function isAlive(): bool ;
    public function repairHealth(): bool ;

    public function getCurrentHarbor(): Harbor ;
    public function setCurrentHarbor($currentHarbor): Ship ;
    public function setState(State $state): Ship ;
    public function setRandom(Random $random): Ship ;
    public function takeShot(Ship $ship): int ;
    public function onShot(int  $damage): void ;
    public function retry(): void ;

}