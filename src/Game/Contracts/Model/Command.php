<?php


namespace BinaryStudioAcademy\Game\Contracts\Model;


interface Command
{
    const ALIAS = null;
    const DESCRIPTION = null;

    public function getAlias(): string ;
    public function getDescription(): string ;
    public function getArguments(): string ;

    public function execute(...$params): string ;
}