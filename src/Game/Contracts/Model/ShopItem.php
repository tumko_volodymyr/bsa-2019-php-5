<?php


namespace BinaryStudioAcademy\Game\Contracts\Model;


interface ShopItem
{
    public function saveOnShip(Ship $ship): string ;
}