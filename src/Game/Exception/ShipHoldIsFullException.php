<?php


namespace BinaryStudioAcademy\Game\Exception;


class ShipHoldIsFullException extends LogicException
{
    public function __construct()
    {
        parent::__construct('Ship hold is full');
    }
}